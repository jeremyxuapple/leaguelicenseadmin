import { NorwallAdminPage } from './app.po';

describe('norwall-admin App', () => {
  let page: NorwallAdminPage;

  beforeEach(() => {
    page = new NorwallAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
