import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../common/form/form-base';
import { FormControlService } from '../../common/form/form.service';
import { InputVariable } from '../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/ApiService';
import { ManagementService } from './management.service';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css'], 
  providers: [ApiService, FormControlService, ManagementService],
})
export class ManagementComponent implements OnInit {
	inputs: Array<any>;
  settingsFields: FormGroup;
  productFields: FormGroup;

  formValid = {};
  
  constructor(
  	private route: ActivatedRoute,
		private apiService: ApiService,
		private fb: FormBuilder,
		private fcs: FormControlService,
		private service: ManagementService
  	) { }

  ngOnInit() {
    this.getSettings();
  } 

  getSettings() {      
   this.apiService.get(_window().storepath + '/api/v1/management', undefined).then(response => {
      let data = response[0]; 
      let settingsFields = this.service.getSettingsFields();
      let form: FormBase<any>[] = [];
      
      for(let field in settingsFields) {
        settingsFields[field].value = data[field];
        form.push(new InputVariable(settingsFields[field]));
      }

      this.inputs = form;
      this.settingsFields = this.fcs.toFormGroup(form);
    });
  }

  saveSettings() {
    this.apiService.put(_window().storepath + '/api/v1/management', this.settingsFields.value).then(response => {
    });
    
  }


}
