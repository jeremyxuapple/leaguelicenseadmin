import { Injectable } from '@angular/core';

@Injectable()
export class ManagementService {
	getSettingsFields() {
      return {
          shipping_markup: {key: 'shipping_markup', label: 'Markup the shipping quotes by: ', required: true },
          inventory_subtraction_value: {key: 'inventory_subtraction_value', label: 'Set inventory subtraction value: ', required: true },
          handling_markup: {key: 'handling_markup', label: 'Markup the handling fee by: ', required: true }, 
          handling_ex_tax: {key: 'handling_ex_tax', label: 'Handling Fee Excluded Tax: ', required: true }, 
          handling_inc_tax: {key: 'handling_inc_tax', label: 'Handling Fee Included Tax: ', required: true }, 
      };
    }

}