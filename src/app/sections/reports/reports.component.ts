import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../common/form/form-base';
import { FormControlService } from '../../common/form/form.service';
import { InputVariable } from '../../common/form/input/input-variable/form.input-variable';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [ApiService, FormControlService]
})
export class ReportsComponent implements OnInit {
	inputs1: object = {};
  dateForm: FormGroup;
  form: FormGroup;

  constructor(
    private apiService: ApiService,
  	private fb: FormBuilder, 
  	private fcs: FormControlService
  	) 
  { 
  	 	/*this.dateForm = this.fb.group({
  			startDate: {key:'startDate', label: 'Start Date', required: true, controlType: 'datepicker'},
  			endDate: {key: 'endDate', label: 'End Date', required: true, controlType: 'datepicker'},
  		});

  		this.inputs1 = this.form;
  		// this.dateForm = this.fcs.toFormGroup(this.form);

  		console.log(this.dateForm);*/
      this.createForm();
  
	}

  createForm() {
    let form: FormBase<any>[] = [];
    const fields = {
      startDate: {key:'startDate', label: 'Start Date', required: true, controlType: 'datepicker'},
      endDate: {key: 'endDate', label: 'End Date', required: true, controlType: 'datepicker'},
    }
    let inputs = {};

    for(let field in fields) {
      const input = new InputVariable(fields[field]);
      form.push(input);
      inputs[fields[field].key] = input;
    }

    this.inputs1 = inputs;
    this.dateForm = this.fcs.toFormGroup(form);
  }

 
  ngOnInit() {
  }

  createStoreReport(term ) {

     let getvars = {
       date_start: this.dateForm.value.startDate,
       date_end: this.dateForm.value.endDate,
       type: term
     }

    let url = _window().storepath + '/api/v1/reports/1/stores?';
    url = url + this.apiService.serialize(getvars, false);
    url = this.apiService.injectToken(url);
    _window().open(url);
  }

  createSubscriptionReport(term) {
    console.log(term);
    let getvars = {
       date_start: this.dateForm.value.startDate,
       date_end: this.dateForm.value.endDate,
       type: term
     }
  }

  createSupplierReport(term) {
    let getvars = {
       date_start: this.dateForm.value.startDate,
       date_end: this.dateForm.value.endDate,
       type: term
     }
  }

}

