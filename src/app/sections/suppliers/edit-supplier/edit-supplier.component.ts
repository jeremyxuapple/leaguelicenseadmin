import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';
import { SupplierService } from './supplier.service';

function _window(): any {
	return window;
}

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.css'],
  providers: [ApiService, FormControlService, SupplierService],
})

export class EditSupplierComponent implements OnInit {
	inputs1: Array<any>;
  inputs2: Array<any>;
  supplierDetails: FormGroup;
  supplierLocation: FormGroup;
  locationCount: number;
  id: number;
  
  constructor(
  		private route: ActivatedRoute,
  		private apiService: ApiService,
      private fb: FormBuilder,
      private fcs: FormControlService,
      private service: SupplierService
  	) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
      this.id = params.id;
  		this.getSupplier();
      // this.getSupplierLocation();
  	});
  }

  supplier = { };
  formValid = { };
  location = [];
  statusOptions = ["good", "bad", "suspend"];
  datafeedOptions = ["Caption Sparrow", "Black Pearl"];

  getSupplier() { 	
  	this.apiService.get(_window().storepath + '/api/v1/suppliers/' + this.id, undefined).then(response => {
      
      this.supplier = response["supplier"];
      let locationData = this.supplier["locations"];
      
      let primaryLocationData = {};

      let primaryLocation = this.service.getSupplierLocation();
      let supplierDetails = this.service.getSupplierFields();
      let form: FormBase<any>[] = [];
      let locationForm: FormBase<any>[] = [];
      
      // Building billing details for the supplier

      for(let field in supplierDetails) {
        supplierDetails[field].value = this.supplier[field];
        form.push(new InputVariable(supplierDetails[field]));
      }

      this.inputs1 = form;
      this.supplierDetails = this.fcs.toFormGroup(form);

       for(let location of locationData) {

        if(location['level'] == 'primary') {
          primaryLocationData = location;
        }

        // let locationFormat = this.service.getSupplierLocation();


        // for(let field in locationFormat) {
        //   locationFormat[field].value = location[field];
        //   let locationForm: FormBase<any>[] = [];
        //   locationForm.push(new InputVariable(locationForm[field]));
        // }
        

    }
      
      // Building location information for the
      for(let field in primaryLocation) {
        primaryLocation[field].value = primaryLocationData[field];
        locationForm.push(new InputVariable(primaryLocation[field]));
      }

      this.inputs2 = locationForm;
      this.supplierLocation = this.fcs.toFormGroup(locationForm);
  	});
  }

  // getSupplierLocation() {
  //   this.apiService.get(_window().storepath + '/api/v1/suppliers/' + this.id + '/locations', undefined).then(response => {
  //     let data = response["locations"];
  //     let locationData = {};
  //     let primaryLocation = this.service.getSupplierLocation();
  //     let form: FormBase<any>[] = [];
     
  //     for (let location of data) {
  //       if(location['status'] == "primary") {
  //         locationData = location;
  //       }
  //     } 

  //     for (let field in primaryLocation) {
  //       primaryLocation[field].value = locationData[field];
  //       form.push(new InputVariable(primaryLocation[field]));
  //     }

  //     this.inputs2 = form;
  //     this.supplierLocation = this.fcs.toFormGroup(form);  
  //   });
  // }

   editSupplier() {
     // let warehouse = { "location": this.supplierLocation.value };
     let location = [];
     location.push(this.supplierLocation.value);

     let Location = {"locations": location};
     console.log(this.supplier);

     let data = Object.assign(this.supplierDetails.value, Location);
     
    //  this.apiService.post(_window().storepath + '/api/v1/suppliers/' + this.id, data).then(response => {

    // });
 }

  addLocation() {
    let formString = 
      `<form class="form-container">
        <h1>Supplier Warehouse</h1>
        <div class="form-input-container">
            <div class="col1" *ngIf="supplierLocation">
                <form [formGroup]="supplierLocation" (ngSubmit)="editSupplier()">
                    <div *ngFor="let input of inputs2" class="form-item">
                        <input-variable [input]="input" [form]="supplierLocation"></input-variable>
                    </div>
                </form>
            </div>
        </div>
      </form>`;

    let locationForm = document.getElementById('location-container'); 
    locationForm.innerHTML = formString; 
 }

}
