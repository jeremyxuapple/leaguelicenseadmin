import { Injectable } from '@angular/core';

@Injectable()
export class SupplierService {

  getSupplierFields() {
  	return {
      name: {key: 'name', label: 'Name ', required: true},
      contact_first_name: {key: 'contact_first_name', label: 'Contact First Name', required: true},
  		contact_last_name: {key: 'contact_last_name', label: 'Contact Last Name ', required: true},
      contact_email: {key: 'contact_email', label: 'Contact Email', required: true },
      contact_phone: {key: 'contact_phone', label: 'Contact Phone', required: true },
      email_return: {key: 'email_return', label: 'Email Return', required: true },
      billing_first_name: {key: 'billing_first_name', label: 'Billing First Name', required: true},
  		billing_last_name: {key: 'billing_last_name', label: 'Billing Last Name', required: true},
  		billing_company: {key: 'billing_company', label: 'Billing Company', required: true}, 
  		billing_address_1: {key: 'billing_address_1', label: 'Billing Address 1', required: true},
  		billing_address_2: {key: 'billing_address_2', label: 'Billing Address 2', required: true},
  		billing_city: {key: 'billing_city', label: 'Billing City', required: true},
  		billing_state: {key: 'billing_state', label: 'Billing State', required: true},
  		billing_zip: {key: 'billing_zip', label: 'Billing Zip', required: true},
  		billing_country: {key: 'billing_country', label: 'Billing Country', required: true},
  		billing_email: {key: 'billing_email', label: 'Billing Email', required: true},
  		billing_phone: {key: 'billing_phone', label: 'Billing Pphone', required: true},
      status: {key: 'status', options: ['Suspended', 'Active'], label: 'Status', required: true , controlType: 'select'},
      data_feed: {key: 'data_feed', options:['Bright Pearl'], label: 'Data Feed', required: true , controlType: 'select'},
      // staff_notes: {key: 'staff_notes', label: 'Staff Notes', required: true},
  	}
  }

  getSupplierLocation() {
  	return {
  		supplier_id: {key: 'supplier_id', label: 'Supplier ID', required: true},
      contact_first_name: {key: 'contact_first_name', label: 'Contact First Name', required: true},
      contact_last_name: {key: 'contact_last_name', label: 'Contact Last Name ', required: true},
      contact_email: {key: 'contact_email', label: 'Contact Email', required: true },
      contact_phone: {key: 'contact_phone', label: 'Contact Phone', required: true },
      address_1: {key: 'address_1', label: 'Address 1', required: true},
  		address_2: {key: 'address_2', label: 'Address 2', required: true},
  		city: {key: 'city', label: 'City', required: true},
  		state: {key: 'state', label: 'State', required: true},
  		country: {key: 'country', label: 'Country', required: true},
      level: {key: 'level', label: 'Level', required: true}
  	}
  }

}
