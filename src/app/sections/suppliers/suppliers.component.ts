import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

function _window(): any {
  return window;
}

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css'],
  providers: [ApiService],
})
export class SuppliersComponent implements OnInit {

  constructor(
  	private apiService: ApiService,
  	) { }

  ngOnInit() {
  	this.getSuppliers();
  }

		dataAvailable = true;
		buffer = 0;
		page = 1;
		maxpages = 1;
		limit = 10;
		query = "";
		suppliers = [];
		sortdirection = 'asc';
		actionbar = false;

		fields = [
			// {type: 'checkbox'},
			{name: 'Supplier Name', col: 'name', basic: true},
			{name: 'Contact First Name', col: 'contact_first_name', basic: true},
			{name: 'Contact Last Name', col: 'contact_last_name', basic: true},
			{name: 'Phone', col: 'contact_phone', basic: true },
			{name: 'Email', col: 'contact_email', basic: true },
			{name: 'Status', col: 'status', basic: true},
			{name: 'Action', actions: {
					options: [
						{name: 'Edit', link: '/suppliers/:id/edit', replacements: {
							'id': 'id'
						}},
					]
				}
			},
		];
		bulkOptions = [
			{name: 'Delete Suppliers', action: 'deleteSuppliers'},
			{name: 'Active Suppliers', action: 'activeSuppliers'}
		];

getSuppliers() {
		const suppliers = this;
		this.buffer++;

		setTimeout(function() {
			suppliers.buffer--;
			if (suppliers.buffer > 0)
				return false;

			let getvars = {
				page: suppliers.page,
				limit: suppliers.limit,
				sortdirection: suppliers.sortdirection,
				query: undefined,
				suppliers: undefined,
			};

			if (suppliers.query.length > 1){
				getvars.query = suppliers.query;
			} else {
				delete getvars.query;
			}

			if (typeof suppliers.suppliers !== 'undefined')
				getvars.suppliers = suppliers.suppliers;

			suppliers.apiService.get(_window().storepath + '/api/v1/suppliers', getvars).then(response => {
				if (response.count == null) suppliers.dataAvailable = false;
				suppliers.suppliers = response.suppliers;
				suppliers.page = response.page;
				suppliers.maxpages = response.maxpages;
				suppliers.limit = response.limit;
			});
		}, 50);
	}

		bulkChange(action) {
			// we have to put all suppliers in an array
			let selected = [];
			for(let k = 0; k < this.suppliers.length; k++) {
				if (this.suppliers[k].selected) {
					selected.push(this.suppliers[k]);
				}
			}

			if (typeof this[action] !== 'undefined')
				this[action](selected);
		}

		deleteSuppliers(suppliers) {

			var promises = [];

			for(let supplier of suppliers){
					promises.push(this.apiService. delete(_window().storepath + '/api/v1/suppliers/' + supplier.id, undefined));		
				}

			Promise.all(promises).then(response => {
				this.getSuppliers();
			});

		}

		activeSuppliers(suppliers) {

			var promises = [];

			for(let supplier of suppliers) {
				
				let promise = this.apiService.put(_window().storepath + '/api/v1/suppliers/' + supplier.id + '/active', undefined);
				promises.push(promise);
			}
			
			Promise.all(promises).then(response => {
				this.getSuppliers();
			});
		}

		filterItems(query) {
			this.query = query;
			this.getSuppliers();
		}

		updateSort(data) {
			this.suppliers = data.order;
			this.sortdirection = data.direction;
		}

		changePage(page: number): void {
			this.page = page;
			this.getSuppliers();
		}
}
