export class Order {
	ID: number;
	order_id: number;
	supplier_id: number;
	billing_id: number;
	shipping_id: number;
	status: string;
	tracking_number: string;
	shipping_fee: number;
	total: number;
	items_total: number;
	items_shipped: number;
}
