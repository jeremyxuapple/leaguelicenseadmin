import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
	selector: 'app-orders',
	templateUrl: './orders.component.html',
	styleUrls: ['./orders.component.css'],
	providers: [ApiService],
})
export class OrdersComponent implements OnInit {

	constructor(
		private apiService: ApiService,
	) { }
	paginationLimit = 10;

	@Input()
	get limit() {
		return this.paginationLimit;
	}
	set limit(val) {
		this.paginationLimit = val;
		
	}

	ngOnInit() {
		this.getOrders();
	}

	// let's define some variables
	dataAvailable = true;
	buffer = 0;
	orders = [];
	page = 1;
	maxpages = 1;
	query = "";
	order = undefined;
	actionbar = false;
	sortdirection = 'asc';
	fields = [
		{name: 'Account ID', col: 'account_id', basic: true },
		{name: 'Date Time', col: 'date_created', basic: true },
		{name: 'Product ID', col: 'product_id', basic: true },
		{name: 'Base Cost Price', col: 'base_cost_price', basic: true },
		{name: 'SKU', col: 'sku', basic: true },
		{name: 'Quantity', col: 'quantity', basic: true },
		{name: 'Tracking Number', col: 'tracking_number', basic: true },
		{name: 'Action', actions: {
				options: [
					{name: 'View', link: '/orders/:id/edit', replacements: {
						'id': 'id'
					}},
				]
			}
		},
	];
	// bulkOptions = [
	// 	{name: 'Delete Orders', action: 'deleteOrders'},
	// ];

getOrders() {
		const orders = this;
		this.buffer++;
		setTimeout(function() {
			orders.buffer--;

			if (orders.buffer > 0)
				return false;

			let getvars = {
				page: orders.page,
				limit: orders.limit,
				sortdirection: orders.sortdirection,
				query: undefined,
				order: undefined,
			};

			if (orders.query.length > 1){
				getvars.query = orders.query;
			} else {
				delete getvars.query;
			}

			if (typeof orders.order !== 'undefined')
				{
					getvars.order = orders.order;
				}
				orders.apiService.get(_window().storepath + '/api/v1/orders', getvars).then(response => {
					if (response.count == null) orders.dataAvailable = false;
					orders.orders = response.orders;
					orders.page = response.page;
					orders.maxpages = response.maxpages;
					orders.limit = response.limit;
			});
		}, 50);
	}

	bulkChange(action) {
		// we have to put all orders in an array
		let selected = [];
		for(let k = 0; k < this.orders.length; k++) {
			if (this.orders[k].selected) {
				selected.push(this.orders[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);
	}

	filterItems(query) {
		this.query = query;
		this.getOrders();
	}

	updateSort(data) {
		this.order = data.order;
		this.sortdirection = data.direction;
		//this.getOrders();
	}

	changePage(page: number): void {
			this.page = page;
			this.getOrders();
	}

}
