import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
	selector: 'app-edit-order',
	templateUrl: './edit-order.component.html',
	styleUrls: ['./edit-order.component.css'],
	providers: [ApiService]
})

export class EditOrderComponent implements OnInit {
	id: number;
	constructor(
		private route: ActivatedRoute,
		private apiService: ApiService,
	) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.id = params.id;
			this.getOrder();
		}); 
	}
	buffer = 0;
	orders = [];
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	sortdirection = 'asc';

	billing = {};
	shipping = {};
	order = {};
	products = [];
	subtotal: number = 0;
	formValid = {};

	getOrder() {
		this.apiService.get(_window().storepath + '/api/v1/orders/' + this.id, undefined).then(response => {
			this.order = response["order"][0];
			// this.billing = this.order["billing_address"]
			// this.shipping = this.order["shipping_addresses"][0];

			// this.products = this.order["products"];
			// for(let product of this.products) {
			// 	var total = parseFloat(product.price_inc_tax);
			// 	this.subtotal += total;
			// }
		});
	}

}
