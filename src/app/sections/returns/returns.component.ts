import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-returns',
  templateUrl: './returns.component.html',
  styleUrls: ['./returns.component.css']
})
export class ReturnsComponent implements OnInit {

  constructor(
  	private apiService: ApiService
  	) { }

  dataAvailable = true;
  returns = [];
  buffer = 0;
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	sortdirection = 'asc';
	notification = undefined;

  fields = [
		{type: 'checkbox'},
		{name: 'Order Id', col: 'order_id', basic: true },
		{name: 'Account Id', col: 'account_id', basic: true },
		{name: 'Supplier Id', col: 'supplier_id', basic: true },
		{name: 'Product Sku', col: 'sku', basic: true },
		{name: 'Product Name', col: 'name', basic: true },
		{name: 'Quantity Returned', col: 'quantity_returned', basic: true },
		{name: 'Reason', col: 'reason', basic: true },
		{name: 'Status', col: 'status', basic: true },
		{name: 'Action', actions: {
				options: [
					{name: 'Edit', link: '/returns/:id/edit', replacements: {
						'id': 'id'
					}},
				]
			}
		},
	];

	bulkOptions = [
		{name: 'Approve Returns', action: 'approveReturns'},
		{name: 'Refuse Returns', action: 'refuseReturns'},
	];

  ngOnInit() {
  	this.getReturns();
  }

  getReturns() {
		const returns = this;
		this.buffer++;
		setTimeout(function() {
			returns.buffer--;

			if (returns.buffer > 0)
				return false;

			let getvars = {
				page: returns.page,
				limit: returns.limit,
				sortdirection: returns.sortdirection,
				query: undefined,
				notification: undefined,
			};

			if (returns.query.length > 1){
				getvars.query = returns.query;
			} else {
				delete getvars.query;
			}

			if (typeof returns.notification !== 'undefined')
				getvars.notification = returns.notification;

			returns.apiService.get(_window().storepath + '/api/v1/returns', getvars).then(response => {
					for(let r of response.returns) {
						if(r.quantity_returned > 0) {
							returns.returns.push(r);
						}
					}		
				returns.page = response.page;
				returns.maxpages = response.maxpages;
				returns.limit = response.limit;
			});
		}, 50);
	}

	bulkChange(action) {
		// we have to put all returns in an array
		let selected = [];
		for(let k = 0; k < this.returns.length; k++) {
			if (this.returns[k].selected) {
				selected.push(this.returns[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);		
	}

	approveReturns(returns) {

		var promises = [];
		var getvars = {
			status: "approved",
		};
		for(let returnProduct of returns){
			let promise = this.apiService.put(_window().storepath + '/api/v1/returns/' + returnProduct.id, getvars);
				promises.push(promise);
		}

		Promise.all(promises).then(response => {
			this.getReturns();
		});
	}

	refuseReturns(returns) {

		var promises = [];

		for(let returnProduct of returns) {
			let promise = this.apiService.put(_window().storepath + '/api/v1/returns/' + returnProduct.id + '/refuse', undefined);
				promises.push(promise);
		}

		Promise.all(promises).then(response => {
			this.getReturns();
		});

	}

	changePage(page: number): void {
			this.page = page;
			this.getReturns();
		}

	filterItems(query) {
		this.query = query;
		this.getReturns();
	}

}
