import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReturnComponent } from './edit-return.component';

describe('EditReturnComponent', () => {
  let component: EditReturnComponent;
  let fixture: ComponentFixture<EditReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
