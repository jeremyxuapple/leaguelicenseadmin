import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';
import { ReturnService } from './return.service';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-edit-return',
  templateUrl: './edit-return.component.html',
  styleUrls: ['./edit-return.component.css'], 
  providers: [ApiService, ReturnService, FormControlService]
})
export class EditReturnComponent implements OnInit {
	id: number;
  return: object;
  returnObject: any;
  inputs: Array<any>;
  returnFormGroup: FormGroup;
  constructor(
  	private route: ActivatedRoute,
  	private apiService: ApiService,
    private returnService: ReturnService, 
    private fcs: FormControlService,
  	) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
  		this.id = params.id;
  		this.getReturn();
  	});
  }

  getReturn(){
  	this.apiService.get(_window().storepath + '/api/v1/returns/' + this.id, undefined ).then(response => {
  		// console.log(response);
      this.returnObject = response["return"];
      let returnFields = this.returnService.getReturnFields();
      let form: FormBase<any>[] = [];

      for (let field in returnFields) {
        returnFields[field].value = this.returnObject[field];
        form.push(new InputVariable(returnFields[field]));
      }

      this.inputs = form;
      this.returnFormGroup = this.fcs.toFormGroup(form);      
  	});
  }

  editReturn() {
    let getvar = this.returnFormGroup.value;
    this.apiService.put(_window().storepath + '/api/v1/returns/' + this.id, getvar).then(response => {
    });
  }

}
