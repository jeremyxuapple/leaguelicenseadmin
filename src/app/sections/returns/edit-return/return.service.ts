import { Injectable } from '@angular/core';

@Injectable()
export class ReturnService {

	getReturnFields() {
		return {
			// account_id: { key: 'account_id', label: 'Account ID: ', required: true },
			// order_id: { key: 'order_id', label: 'Order ID: ', required: true }, 
			// supplier_id: { key: 'supplier_id', label: 'Supplier ID: ', required: true }, 
			// product_name: { key: 'product_name', label: 'Product Name: ', required: true}, 
			// sku: { key: 'sku', label: 'Product Sku: ', required: true }, 
			// quantity: { key: 'quantity', label: 'Returned Quantity: ', required: true },
			// reason: { key: 'reason', label: 'Returned Reason: ', required: true },
			// total_ex_tax: { key: 'total_ex_tax', label: 'Total Exclusive Tax: ', required: true }, 
			// total_inc_tax: { key: 'total_inc_tax', label: 'Total Inclusive Tax: ', required: true }, 
			// total_tax: { key: 'total_tax', label: 'Total Tax: ', required: true },
			// type: { key: 'type', label: 'Request Type: ', required: true },
			status: { key: 'status', options: ["Wait For Approval", "Approved", "Declined"], label: 'Status: ', required: true, controlType: 'select' }, 
		}
	}
  constructor() { }

}
