import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-warnings',
  templateUrl: './warnings.component.html',
  styleUrls: ['./warnings.component.css']
})
export class WarningsComponent implements OnInit {

  constructor(
  		private apiService: ApiService,
  	) { }

  warnings = [];
  buffer = 0;
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	sortdirection = 'asc';
	notification = undefined;

  fields = [
		{type: 'checkbox'},
		{name: 'ID', col: 'id', basic: true},
		{name: 'Time', col: 'date_modified', basic: true },
		{name: 'Product Name', col: 'product_name', basic: true },
		{name: 'Status', col: 'status', basic: true},
	];

	bulkOptions = [
		{name: 'Delete Warnings', action: 'deleteWarnings'},
		{name: 'Activate Warnings', action: 'activeWarnings'},
	];
  ngOnInit() {
  	this.getWarnings();
  }

  getWarnings() {
		const warnings = this;
		this.buffer++;
		setTimeout(function() {
			warnings.buffer--;

			if (warnings.buffer > 0)
				return false;

			let getvars = {
				page: warnings.page,
				limit: warnings.limit,
				sortdirection: warnings.sortdirection,
				query: undefined,
				notification: undefined,
			};

			if (warnings.query.length > 1){
				getvars.query = warnings.query;
			} else {
				delete getvars.query;
			}

			if (typeof warnings.notification !== 'undefined')
				getvars.notification = warnings.notification;

			warnings.apiService.get(_window().storepath + '/api/v1/warnings', getvars).then(response => {
				warnings.warnings = response.items;
				warnings.page = response.page;
				warnings.maxpages = response.maxpages;
				warnings.limit = response.limit;
			});
		}, 50);
	}

	bulkChange(action) {
		// we have to put all orders in an array
		let selected = [];
		for(let k = 0; k < this.warnings.length; k++) {
			if (this.warnings[k].selected) {
				selected.push(this.warnings[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);
	}

	deleteWarnings(warnings) {
		for(let warning of warnings){
			this.apiService.delete(_window().storepath + '/api/v1/warnings/' + warning.id, undefined);
			}
	}

	activeWarnings(warnings) {
			for(let warning of warnings) {
				this.apiService.put(_window().storepath + '/api/v1/warnings/' + warning.id + '/active', undefined)
			}
			// this.getSuppliers();
			// location.reload();
		}

		changePage(page: number): void {
			this.page = page;
			this.getWarnings();
	}

	filterItems(query) {
		this.query = query;
		this.getWarnings();
	}
	
}
