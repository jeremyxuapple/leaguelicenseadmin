import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

function _window(): any {
  return window;
}

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css'], 
  providers: [ApiService],
})
export class AccountsComponent implements OnInit {

  constructor(
  	private apiService: ApiService,
  	) { }
  actionbar: true;
  ngOnInit() {
  	this.getAccounts();
  }

  dataAvailable = true;
  buffer = 0;
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	sortdirection = 'asc';
	accounts = undefined;

	fields = [
		{type: 'checkbox'},
		{name: 'ID', col: 'account_id', basic: true},
		{name: 'Platform', col: 'platform', basic: true},
		{name: 'Store Name', col: 'name', basic: true},
		{name: 'Join Date', col: 'date_created', basic: true },
		{name: 'Subscription Level', col: 'active_subscription_level', basic: true},
		{name: 'Lifetime Orders', col: 'life_time_orders', basic: true}, 
		{name: 'Status', col: 'status', basic: true},
		{name: 'Zip', col: 'billing_zip', basic: true}, 
		{name: 'Action', actions: {
				options: [
					{name: 'Edit', link: '/accounts/:id/edit', replacements: {
						'id': 'account_id'
					}},
				]
			}
		},
	];

bulkOptions = [
		{name: 'Delete Accounts', action: 'deleteAccounts'},
		{name: 'Activate Accounts', action: 'activateAccounts'},
	];

  getAccounts() {
		const accounts = this;
		this.buffer++;

		setTimeout(function() {
			accounts.buffer--;
			if (accounts.buffer > 0)
				return false;

			let getvars = {
				page: accounts.page,
				limit: accounts.limit,
				sortdirection: accounts.sortdirection,
				query: undefined,
				account: undefined,
			};

			if (accounts.query.length > 1){
				getvars.query = accounts.query;
			} else {
				delete getvars.query;
			}

			if (typeof accounts.accounts !== 'undefined')
				getvars.account = accounts.accounts;

			accounts.apiService.get(_window().storepath + '/api/v1/accounts', getvars).then(response => {
				if (response.count == null) accounts.dataAvailable = false;
				accounts.accounts = response.accounts;
				accounts.page = response.page;
				accounts.maxpages = response.maxpages;
				accounts.limit = response.limit;			
			});
		}, 0) ;
	}

	changePage(page: number): void {
			this.page = page;
			this.getAccounts();
	}

	bulkChange(action) {
		// we have to put all accounts in an array
		let selected = [];
		for(let k = 0; k < this.accounts.length; k++) {
			if (this.accounts[k].selected) {
				selected.push(this.accounts[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);
	}

	deleteAccounts(accounts) {

		var promises = [];

		for(let account of accounts){
			let promise = this.apiService.delete(_window().storepath + '/api/v1/accounts/' + account.account_id, undefined);
			promises.push(promise);
		}

		Promise.all(promises).then(response => {
			this.getAccounts();
		});
	}

	activateAccounts(accounts) {

		var promises = [];

			for(let account of accounts) {
				let promise = this.apiService.put(_window().storepath + '/api/v1/accounts/' + account.account_id + '/active', undefined);
				promises.push(promise);
			}

		Promise.all(promises).then(response => {
			this.getAccounts();
		});
}
	filterItems(query) {
		this.query = query;
		this.getAccounts();
	}
}
