import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

 function _window(): any {
 	return window;
 }

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css'], 
  providers: [ApiService, FormControlService]
})
export class EditAccountComponent implements OnInit {
	id: number;
  inputs: Array<any>;
  accountUpdate: FormGroup;
  
  constructor(
  	private apiService: ApiService,
  	private route: ActivatedRoute,
    private fb: FormBuilder,
    private fcs: FormControlService,
  	) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
  		this.id = params.id;
  		this.getAccount();
  	});
  }

  getUpdateFields() {
    return {
      staff_notes: {key: 'staff_notes', label: 'Staff Notes'}, 
      status: {key: 'status', label: 'Status', options: ['Active', 'Suspended'], controlType: 'select'},
    }
  }

  update = {};
  account = {};
  formValid = {};
  subscriptions = [];
  // statusOptions = ["good", "suspended", "not approved"];
  // PlatformOptions = ["BigCommerce", "Shopify"];
	
  getAccount() {
  	this.apiService.get(_window().storepath + '/api/v1/accounts/' + this.id, undefined). then(response => { 
      this.account = response.account;      
      this.update["staff_notes"] = this.account["staff_notes"];
      this.update["status"] = this.account["status"];

      let updateFields = this.getUpdateFields();
      let form: FormBase<any>[] = []; 

      for(let field in updateFields) {
        updateFields[field].value = this.update[field];
        form.push(new InputVariable(updateFields[field]));
      }

      this.inputs = form;
      this.accountUpdate = this.fcs.toFormGroup(form);

  		this.subscriptions = this.account["subscriptions"];
  	});
  }


  updateAccount() {
    
    this.account["staff_notes"] = this.accountUpdate.value["staff_notes"];
    this.account["status"] = this.accountUpdate.value["status"];
    delete this.account["subscriptions"];
    delete this.account["id"];
    delete this.account["date_modified"];

    // console.log(this.account);
    this.apiService.put(_window().storepath + '/api/v1/accounts/' + this.id, this.accountUpdate.value). then(response => {

    });
  }


}
