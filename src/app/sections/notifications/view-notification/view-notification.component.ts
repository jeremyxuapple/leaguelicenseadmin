import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

function _window(): any {
	return window;
}

@Component({
  selector: 'app-view-notification',
  templateUrl: './view-notification.component.html',
  styleUrls: ['./view-notification.component.css'], 
  providers: [ApiService]
})

export class ViewNotificationComponent implements OnInit {
	id: number;
  msgType: string;
  updateTime: string;
  entityName: string;

  constructor(
  	private route: ActivatedRoute,
  	private apiService: ApiService,
  	) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
  		this.id = params.id;
  	});
    this.getNotification();
  }

  getNotification() {
    this.apiService.get(_window().storepath + '/api/v1/notifications/' + this.id, undefined). then(response => {
      this.entityName = response.entity_name;
      this.msgType = response.msg_type;
      this.updateTime = response.date_modified;
      // this.account = response.account;
      // this.subscriptions = this.account["subscriptions"];
    });
  }

}
