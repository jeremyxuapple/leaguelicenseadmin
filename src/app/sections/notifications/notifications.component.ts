import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor(
  		private apiService: ApiService,
  	) { }

  dataAvailable = true;
  notifications = [];
  buffer = 0;
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	sortdirection = 'desc';
	notification = undefined;

  fields = [
		{type: 'checkbox'},
		{name: 'ID', col: 'id', basic: true},
		{name: 'Type', col: 'msg_type', basic: true},
		{name: 'Time', col: 'date_modified', basic: true },
		{name: 'Name **', col: 'entity_name', basic: true },
		{name: 'Status', col: 'status', basic: true},
		{name: 'Detail', col: 'details', basic: true},	
		// {name: 'Action', actions: {
		// 		options: [
		// 			{name: 'Edit', link: '/notifications/:id/view', replacements: {
		// 				'id': 'id'
		// 			}},
		// 		]
		// 	}
		// },
	];
  ngOnInit() {
  	this.getNotifications();
  }

 //  bulkOptions = [
	// 	{name: 'Delete Notifications', action: 'deleteNotifications'},
	// 	{name: 'Activate Notifications', action: 'activeNotifications'},
	// ];

  getNotifications() {
		const notifications = this;
		this.buffer++;
		setTimeout(function() {
			notifications.buffer--;

			if (notifications.buffer > 0)
				return false;

			let getvars = {
				page: notifications.page,
				limit: notifications.limit,
				order_sort: notifications.sortdirection,
				query: undefined,
				order_option: "id"
				
			};

			if (notifications.query.length > 1){
				getvars.query = notifications.query;
			} else {
				delete getvars.query;
			}

			// if (typeof notifications.notification !== 'undefined')
			// 	getvars.notification = notifications.notification;

			notifications.apiService.get(_window().storepath + '/api/v1/notifications', getvars).then(response => {
				if(response.count == null) this.dataAvailable = false;
				notifications.notifications = response.notifications;
				notifications.page = response.page;
				notifications.maxpages = response.maxpages;
				notifications.limit = response.limit;
			});
		}, 50);
	}

	bulkChange(action) {
		// we have to put all orders in an array
		let selected = [];
		for(let k = 0; k < this.notifications.length; k++) {
			if (this.notifications[k].selected) {
				selected.push(this.notifications[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);
	}

	deleteNotifications(notifications) {
	  var promises = [];
		for(let notification of notifications){
		let promise = this.apiService.delete(_window().storepath + '/api/v1/notifications/' + notification.id, undefined);
		promises.push(promise);
		}
		
		Promise.all(promises).then(response => {
			this.getNotifications();
		});
	}

	activeNotifications(notifications) {
		var promises = [];
			for(let notification of notifications) {
				let promise = this.apiService.put(_window().storepath + '/api/v1/notifications/' + notification.id + '/active', undefined)
				promises.push(promise);	
			}
			Promise.all(promises).then(response => {
				this.getNotifications();
			})
		}

	changePage(page: number): void {
		this.page = page;
		this.getNotifications();
	}

	filterItems(query) {
		this.query = query;
		this.getNotifications();
	}
}
