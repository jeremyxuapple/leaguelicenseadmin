import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ApiService],
})
export class ProductsComponent implements OnInit {

  constructor(
        private apiService: ApiService,
        private router: Router,
    ) { }

    ngOnInit() {
        this.getProducts();
    }

    // let's define some variables
    products = [];
    page = 1;
    maxpages = 1;
    limit = 10;
    query = "";
    order = undefined;

    fields = [
        {type: 'checkbox'},
        {name: 'Status', col: 'availability', basic: true},
        {name: 'Product SKU', col: 'sku', basic: true},
        {name: 'Stock Level', col: 'inventory_level', basic: true},
        {name: 'Product Name', col: 'name', basic: true},
        {name: 'Price**', col: 'price', basic: true},
        {name: 'Action', actions: {
                options: [
                    {name: 'Edit', link: '/products/:id/edit', replacements: {
                        'id': 'product_id'
                    }}
                ]
            }
        },
    ];
    // bulkOptions = [
    //     {name: 'Delete Orders', action: 'deleteOrders'},
    //     {name: 'Fullfill Orders', action: 'fulfillOrders'},
    // ];

    actionButtons = [
        {name: 'Add', action: 'addProduct'}
    ]

    getProducts() {
        let getvars = {
            page: this.page,
            limit: this.limit,
            query: undefined,
            order: undefined,
        };

        if (this.query.length > 1) {
            getvars.query = this.query;
        } else {
            delete getvars["query"];
        }

        if (typeof this.order !== 'undefined')
            getvars.order = this.order;

        this.apiService.get(_window().storepath + '/api/v1/products', getvars).then(response => {
            this.products = response.products;
            this.page = response.page;
            this.maxpages = response.maxpages;
            this.limit = response.limit;
        });
    }

    changePage(page: number): void {
                this.page = page;
                this.getProducts();
        }

    bulkChange(action) {
        // we have to put all orders in an array
        let selected = [];
        for(let k = 0; k < this.products.length; k++) {
            if (this.products[k].selected) {
                selected.push(this.products[k]);
            }
        }

        if (typeof this[action] !== 'undefined')
            this[action](selected);
    }

    doAction(action) {
        if (typeof this[action] !== 'undefined')
            this[action]();
    }

    addProduct() {
        // we're not navigating to a angular page, we're pushing to the storefront and logging in
        _window().open(_window().storepath + '/api/v1/app/productredirect')
        //this.router.navigate(['/products/add']);
    }

  }
