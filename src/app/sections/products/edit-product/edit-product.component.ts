import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

function _window(): any {
	return window;
}

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
  providers: [ApiService]
})
export class EditProductComponent implements OnInit {
	id: number;

  constructor(
  	private route: ActivatedRoute,
  	private apiService: ApiService
  	) { }

  formValid = [];
  product = {};
  suppliers = [];
  
  status = ["Selling", "Not Selling"];
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.getProduct();
    });
    this.getSupplier();
  }

   getProduct() { 	
  	this.apiService.get(_window().storepath + '/api/v1/products/' + this.id, undefined).then(response => {
      this.product = response;
  	});
  }

  getSupplier() {
    this.apiService.get(_window().storepath + '/api/v1/products/' + this.id + '/supplier', undefined).then(response => {
      for (let supplier of response) {
        this.suppliers.push(supplier[0].name);
      }
    })
  }

}
