import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import some components to route to
import { HomeComponent } from '../home/home.component';
import { AccountsComponent } from '../sections/accounts/accounts.component'
import { EditAccountComponent } from '../sections/accounts/edit-account/edit-account.component';
import { OrdersComponent } from '../sections/orders/orders.component';
import { EditOrderComponent } from '../sections/orders/edit-order/edit-order.component';
import { ProductsComponent } from '../sections/products/products.component';
import { EditProductComponent } from '../sections/products/edit-product/edit-product.component';
import { AddProductsComponent } from '../sections/products/add-products/add-products.component';
import { ReportsComponent } from '../sections/reports/reports.component';
import { InvoicesComponent } from '../sections/invoices/invoices.component';
import { NotificationsComponent } from '../sections/notifications/notifications.component';
import { WarningsComponent } from '../sections/warnings/warnings.component';
import { ViewNotificationComponent } from '../sections/notifications/view-notification/view-notification.component';
import { ManagementComponent } from '../sections/management/management.component';
import { ReturnsComponent } from '../sections/returns/returns.component';
import { EditReturnComponent } from '../sections/returns/edit-return/edit-return.component';
import { SupportComponent } from '../sections/support/support.component';
import { SuppliersComponent } from '../sections/suppliers/suppliers.component';
import { EditSupplierComponent } from '../sections/suppliers/edit-supplier/edit-supplier.component';

// let's set the main routes here
const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'accounts', component: AccountsComponent },
        { path: 'accounts/:id/edit', component: EditAccountComponent },
    { path: 'suppliers', component: SuppliersComponent },
        { path: 'suppliers/:id/edit', component: EditSupplierComponent },
    { path: 'orders', component: OrdersComponent },
        { path: 'orders/:id/edit', component: EditOrderComponent },
    { path: 'products', component: ProductsComponent },
        { path: 'products/add', component: AddProductsComponent },
        { path: 'products/:id/edit', component: EditProductComponent },
    { path: 'reports', component: ReportsComponent },
    { path: 'invoices', component: InvoicesComponent },
    { path: 'notifications', component: NotificationsComponent },
        { path: 'notifications/:id/view', component: ViewNotificationComponent },
    { path: 'warnings', component: WarningsComponent },
    { path: 'management', component: ManagementComponent },
    { path: 'returns', component: ReturnsComponent },
        { path: 'returns/:id/edit', component: EditReturnComponent },
    { path: 'support', component: SupportComponent },
    { path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRouterModule {

}