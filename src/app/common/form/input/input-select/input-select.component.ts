import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.css']
})
export class InputSelectComponent implements OnInit {

 // define inputs
	modelValue = '';
	
	// model for the input
	@Output() modelChange: EventEmitter<any> = new EventEmitter();
	@Input() 
	get model() {
		return this.modelValue;
	}

	set model(val) {
		this.modelValue = val;
		this.modelChange.emit(this.modelValue);	
	}

	@Input() label: string;
	@Input() options: Array<any>;
	@Input() value: string;

	constructor() { }

  ngOnInit() {
  }
}
