import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';

@Component({
  selector: 'input-textarea',
  templateUrl: './input-textarea.component.html',
  styleUrls: ['./input-textarea.component.css']
})
export class InputTextareaComponent implements OnInit {
	// define inputs
	modelValue = '';

	// model for the input
	@Output() modelChange: EventEmitter<any> = new EventEmitter();
	@Input() 
	get model() {
		return this.modelValue;
	}

	set model(val) {
		this.modelValue = val;
		this.modelChange.emit(this.modelValue);
	}

	@Input() rows: number;
	@Input() cols: number;
	@Input() label: string;

  constructor() { }

  ngOnInit() {
  }

}
