import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputVariableComponent } from './input-variable.component';

describe('InputVariableComponent', () => {
  let component: InputVariableComponent;
  let fixture: ComponentFixture<InputVariableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputVariableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputVariableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
