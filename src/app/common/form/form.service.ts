import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { FormBase } from './form-base';

@Injectable()
export class FormControlService {
    constructor() {}

    toFormGroup(forms: FormBase<any>[]) {
        let group: any = {};

        forms.forEach(form => {
            group[form.key] = form.required ? new FormControl(form.value || '', Validators.required)
                                            : new FormControl(form.value || '');
        });

        return new FormGroup(group);
    }
    
    toPostQuery(form: FormGroup, inputs: any) {
        // first we loop through all the inputs
        let query = {};
        for(let field in inputs) {
            let input = inputs[field];
            let value = form.controls[input.key].value;
            query[input.key] = value;
        }

        return query;
    }
}

