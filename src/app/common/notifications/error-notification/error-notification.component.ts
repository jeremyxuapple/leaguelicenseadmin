import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
    selector: 'app-error-notification',
    templateUrl: './error-notification.component.html',
    styleUrls: ['./error-notification.component.css']
})
export class ErrorNotificationComponent implements OnInit {

    constructor(
        public snackBar: MdSnackBar
    ) { }

    ngOnInit() {
    }

}
