import { Injectable } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { ErrorNotificationComponent } from './error-notification.component';

@Injectable()
export class ErrorNotificationService {
    constructor(
        public snackBar: MdSnackBar
    ) { }

    showError(message) {
        /*this.snackBar.openFromComponent(ErrorNotificationComponent, {
            duration: 100000,
            announcementMessage: message,
            extraClasses: ['error-toast'],
        });*/
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            extraClasses: ['error-toast'],
        });
    }

    showSuccess(message) {
        this.snackBar.open(message, 'Close', {
            duration: 5000,
            extraClasses: ['success-toast'],
        });
    }
}