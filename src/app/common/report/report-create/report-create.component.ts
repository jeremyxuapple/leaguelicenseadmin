import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'report-create',
  templateUrl: './report-create.component.html',
  styleUrls: ['./report-create.component.css']
})
export class ReportCreateComponent implements OnInit {

  constructor() { }

  term: string
  @Input() label: string;

  @Output() termChange: EventEmitter<any> = new EventEmitter();
  
  get type() {
    return this.term;
  }

  set type(term) {
    this.term = term;
    this.termChange.emit(this.term);
  }

  ngOnInit() {
  }

  createReport() {
  	
  }

}
