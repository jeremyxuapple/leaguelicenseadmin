import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'report-datepicker',
  templateUrl: './report-datepicker.component.html',
  styleUrls: ['./report-datepicker.component.css']
})
export class ReportDatepickerComponent implements OnInit {

	date = '';

  constructor() { }

  @Input() label: string;
  @Input() setDateLabel: string;

  @Output() setDate: EventEmitter<any> = new EventEmitter();

  @Input()
  	get modelDate() {
  		return this.date;
  	}

  	set modeldate(val) {
  		this.date = val;
  		this.setDate.emit(this.date);
  	}

  ngOnInit() {

  }

}
