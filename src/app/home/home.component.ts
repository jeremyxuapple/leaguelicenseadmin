import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiService } from '../services/ApiService';
import { Order } from '../sections/orders/order';

function _window(): any {
  return window;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApiService],
})
export class HomeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,   
  ) { }

// Define some variables for use

  orders = [];
  notifications = [];
  warnings = [];

  ngOnInit() {
    this.callApi('get');
  }

  changeRoute(route) {
    console.log('new route', route);
      //this.router.navigateByUrl(route);
  }

  callApi($type) {
    
    this.apiService[$type](_window().storepath + '/api/v1/orders', {}).then(response => {
      this.orders = response.orders;
    });

    this.apiService[$type](_window().storepath + '/api/v1/notifications', {}).then(response => {
      this.notifications = response.notifications;
    });

    // this.apiService[$type](_window().storepath + '/api/v1/warnings', {}).then(response => {
    //   this.warnings = response.items;
    // });
  }
}
