import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } 	from '@angular/router';
import { ApiService } from './services/ApiService';

// define the nav links

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	providers: [ ApiService ],
})
export class AppComponent {
	constructor(
		private apiService: ApiService,
	){}

	title = 'app works!';

	// callApi($type) {
	// 	 console.log('calling API', $type);
	// 	this.apiService[$type]('http://leaguelicence.dev/api/v1/orders', {}).then(response => {
	// 		 console.log('response', response);
	// 	});
	// }

	// define nav links
	navLinks = [
		{ name: 'Home', url: 'home' },
		{ name: 'Accounts', url: 'accounts' },
		{ name: 'Suppliers', url: 'suppliers' },		
		{ name: 'Orders', url: 'orders' },
		// { name: 'Products', url: 'products'},
		// { name: 'Reports', url: 'reports'},
		{ name: 'Notifications', url: 'notifications' },
		// { name: 'Warnings', url: 'warnings' },
		{ name: 'App Management', url: 'management' },
		{ name: 'Returns', url: 'returns' },
		// { name: 'FAQ/Support', url: 'support'},
	]
}
