import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Jsonp, URLSearchParams } from '@angular/http';
import { ErrorNotificationService } from '../common/notifications/error-notification/error-notification.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


// global variables
//import { WindowRef } from '../WindowRef';
function _window(): any {
  // return the native window obj
  return window;
}

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
	constructor(
		private http: Http,
		private jsonp: Jsonp,
		private errorService: ErrorNotificationService,
	) { }

	private headers = new Headers({
		// 'Content-Type': 'application/json',
		'Content-Type': 'application/x-www-form-urlencoded',
		'Accept': 'application/json',
		// authorization doesn't work over SSL so we're pushing the token as a get parameter
		//'Authorization': 'Bearer ' + _window().token
	});

	private options = new RequestOptions({ headers: this.headers });

	post(url: string, data: any): Promise<any> {
		if (typeof url === 'undefined')
			throw new Error("Please pass a URL to the ApiService");

		url = this.injectToken(url);
		
		let query = new URLSearchParams(this.serialize(data, false));
 		return this.http.post(url, query, this.options)
			.toPromise()
			.then(response => {
				return this.handleSuccess(response);
			})
			.catch(this.handleError);
	}

		get(url: string, query: object): Promise<any> {
				// console.log('request', url, query);
				if (typeof query != 'undefined')
				url += '?' + this.serialize(query, false);
		
		if (typeof url === 'undefined')
			throw new Error("Please pass a URL to the ApiService");

		url = this.injectToken(url);

		// return this.jsonp.get()

		return this.http.get(url, this.options)
			.toPromise()
			.then(response => {
				return this.handleSuccess(response);
			})
			.catch(this.handleError);
	}

	put(url: string, data: any = undefined): Promise<any> {
		
		if (typeof url === 'undefined')
			throw new Error("Please pass a URL to the ApiService");

		url = this.injectToken(url);

		let query = new URLSearchParams(this.serialize(data, false));
 		return this.http.put(url, query, this.options)
			.toPromise()
			.then(response => {
				return this.handleSuccess(response);
			})
			.catch(this.handleError);
	}

	delete(url: string, data: object): Promise<any> {
		// console.log(url);
		if (typeof url === 'undefined')
			throw new Error("Please pass a URL to the ApiService");

		url = this.injectToken(url);
		
		return this.http.delete(url, JSON.stringify(data))
			.toPromise()
			.then(response => {
				return this.handleSuccess(response);
			})
			.catch(this.handleError);
	}
		// let's make a few observable functions
	oget(url: string, query: any): Observable<any> {
		if (typeof query != 'undefined')
			url += '?' + this.serialize(query, false);
		
		if (typeof url === 'undefined')
			throw new Error("Please pass a URL to the ApiService");

		url = this.injectToken(url);

		return this.http.get(url)
			.map(this.extractData)
			.catch(this.handleObservableError);
	}

	private extractData(res: Response | any) {
		let body = res.json();
		return body.data || {};
	}

	private handleObservableError(error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}

		return Observable.throw(errMsg);
	}

	injectToken(url: string): string {
		// let's add the token to the url string
		if (url.indexOf('?') > -1) {
			url += '&access_token='+_window().token;
		} else {
			url += '?access_token='+_window().token;
		}
		
		return url;
	}


	private handleSuccess(response: any): Promise<any> {
		const body = JSON.parse(response._body);
		let message = 'An Error has occured';
		if (body.error || body.Error) {
			if (body.error) {
				message = body.message;
			} else {
				message = body.Error;
			}

			// let's show the error
			if (message !== 'Unable to find request')
				this.errorService.showError(message);
		}
		else if (body.success || body.Success) {
			message = "Success!";
			this.errorService.showSuccess(message);
		}
		return Promise.resolve(JSON.parse(response._body) || {});
	}

	private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}

	serialize(query: any, prefix: any): any {
		let str = [];
		for(let p in query) {
			if (query.hasOwnProperty(p)) {
						var k = prefix ? prefix + "[" + p + "]" : p, v = query[p];
						str.push((v !== null && typeof v === "object") ?
						this.serialize(v, k) :
						k + "=" + encodeURIComponent(v));
					}
		}
		return str.join("&");
	}
}